<?php
/*
Element Description: VC Give Forms Slider
*/

// Element Class
class vcGiveFormsSlider extends WPBakeryShortCode {

    // Element Init
    function __construct() {
        global $__VcShadowWPBakeryVisualComposerAbstract;
        add_action( 'init', array( $this, 'vc_give_forms_slider_mapping' ) );
        $__VcShadowWPBakeryVisualComposerAbstract->addShortCode('vc_give_forms_slider', array( $this, 'vc_give_forms_slider_html' ));
    }

    // Element Mapping
    public function vc_give_forms_slider_mapping() {

        // Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }

        // Map the block with vc_map()
        vc_map(
          array(
            'name' => __('Give Forms Slider', 'dream'),
            'base' => 'vc_give_forms_slider',
            'description' => __('Give forms slider (carousel)', 'dream'),
            'category' => __('Give', 'dream'),
            'icon' => get_template_directory_uri() . '/framework-customizations/extensions/custom-js-composer/images/give-donations-icon.png',
            'params' => array(
              array(
                'type' => 'textfield',
                'heading' => __('Number of Posts to Show', 'dream'),
                'param_name' => 'number_posts_show',
                'value' => '5',
                'admin_label' => true,
                'group' => 'Source',
              ),
              array(
                'type' => 'dropdown',
                'heading' => __( 'Data Type', 'dream' ),
                'param_name' => 'data_type',
                'value' => array(
                  __('Recent', 'dream') => 'recent',
                  __('By ID', 'dream') => 'by_id',
                ),
                'std' => 'recent',
                'description' => __( 'Select a post data type', 'dream' ),
                'admin_label' => true,
                'group' => 'Source',
              ),
              array(
                'type' => 'textfield',
                'heading' => esc_html__( 'Forms ID', 'dream' ),
                'param_name' => 'forms_id',
                'dependency' => array(
                  'element' => 'data_type',
                  'value' => 'by_id',
                ),
                'value' => '',
                'description' => __('Enter form id you would like show (Ex: 1,2,3).', 'dream'),
                'group' => 'Source',
              ),
              array(
          			'type' => 'el_id',
          			'heading' => __( 'Element ID', 'dream' ),
          			'param_name' => 'el_id',
          			'description' => __( 'Enter element ID .', 'dream' ),
                'group' => 'Source',
              ),
          		array(
          			'type' => 'textfield',
          			'heading' => __( 'Extra class name', 'dream' ),
          			'param_name' => 'el_class',
          			'description' => __( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'dream' ),
                'group' => 'Source',
              ),
              /*** Layout Options ***/
              array(
                'type' => 'dropdown',
                'heading' => __('Image Size', 'dream'),
                'param_name' => 'image_size',
                'value' => array(
                  array('value' => 'thumbnail', 'label' => esc_html__('Thumbnail', 'dream')),
                  array('value' => 'medium', 'label' => esc_html__('Medium', 'dream')),
                  array('value' => 'medium_large', 'label' => esc_html__('Medium Large', 'dream')),
                  array('value' => 'large', 'label' => esc_html__('Large', 'dream')),
                  array('value' => 'dream-image-large', 'label' => esc_html__('Large (1228 x 691)', 'dream')),
                  array('value' => 'dream-image-medium', 'label' => esc_html__('Medium (614 x 346)', 'dream')),
                  array('value' => 'dream-image-small', 'label' => esc_html__('Small (295 x 166)', 'dream')),
                  array('value' => 'dream-image-square-800', 'label' => esc_html__('Square (800 x 800)', 'dream')),
                  array('value' => 'dream-image-square-300', 'label' => esc_html__('Square (300 x 300)', 'dream')),
                ),
                'std' => 'dream-image-medium',
                'description' => __('Select a image size', 'dream'),
                'group' => 'Layout',
              ),
              array(
                'type' => 'vc_image_picker',
                'heading' => __( 'Select Layout', 'dream' ),
                'param_name' => 'layout',
                'value' => array(
                  'default' => get_template_directory_uri() . '/framework-customizations/extensions/custom-js-composer/images/layouts/give-forms-slider-default.jpg',
                  'style-1' => get_template_directory_uri() . '/framework-customizations/extensions/custom-js-composer/images/layouts/give-forms-slider-layout-2.jpg',
                  'style-2' => get_template_directory_uri() . '/framework-customizations/extensions/custom-js-composer/images/layouts/give-forms-slider-layout-3.jpg',
				          'style-3' => get_template_directory_uri() . '/framework-customizations/extensions/custom-js-composer/images/layouts/give-forms-slider-layout-4.jpg',
				          'style-4' => get_template_directory_uri() . '/framework-customizations/extensions/custom-js-composer/images/layouts/give-forms-slider-layout-5.jpg',
				          'style-5' => get_template_directory_uri() . '/framework-customizations/extensions/custom-js-composer/images/layouts/give-forms-slider-layout-6.jpg',
                  'style-6' => get_template_directory_uri() . '/framework-customizations/extensions/custom-js-composer/images/layouts/give-forms-slider-layout-7.jpg',
                  'style-7' => get_template_directory_uri() . '/framework-customizations/extensions/custom-js-composer/images/layouts/posts-slider-layout-church.png',
                ),
                'std' => 'default',
                'description' => __('Select a layout display', 'dream'),
                'group' => 'Layout',
              ),
              /*** Slider Options ***/
              array(
                'type' => 'textfield',
                'heading' => __('Items', 'dream'),
                'param_name' => 'items',
                'value' => '1',
                'admin_label' => false,
                'description' => __('The number of items you want to see on the screen.', 'dream'),
                'group' => 'Slider Options',
              ),
              array(
                'type' => 'textfield',
                'heading' => __('Margin', 'dream'),
                'param_name' => 'margin',
                'value' => '0',
                'admin_label' => false,
                'description' => __('margin-right(px) on item.', 'dream'),
                'group' => 'Slider Options',
              ),
              array(
                'type' => 'dropdown',
                'heading' => __('Loop', 'dream'),
                'param_name' => 'loop',
                'value' => array(
                  __('Yes', 'dream') => '1',
                  __('No', 'dream') => '0',
                ),
                'std' => '0',
                'description' => __('Infinity loop. Duplicate last and first items to get loop illusion.', 'dream'),
                'group' => 'Slider Options',
              ),
              array(
                'type' => 'dropdown',
                'heading' => __('Center', 'dream'),
                'param_name' => 'center',
                'value' => array(
                  __('Yes', 'dream') => '1',
                  __('No', 'dream') => '0',
                ),
                'std' => '0',
                'description' => __('Center item. Works well with even an odd number of items.', 'dream'),
                'group' => 'Slider Options',
              ),
              array(
                'type' => 'textfield',
                'heading' => __('stagePadding', 'dream'),
                'param_name' => __('stage_padding', 'dream'),
                'value' => '0',
                'description' => __('Padding left and right on stage (can see neighbours).', 'dream'),
                'group' => 'Slider Options',
              ),
              array(
                'type' => 'textfield',
                'heading' => __('startPosition', 'dream'),
                'param_name' => __('start_position', 'dream'),
                'value' => '0',
                'description' => __('Start position or URL Hash string like `#id`.', 'dream'),
                'group' => 'Slider Options',
              ),
              array(
                'type' => 'dropdown',
                'heading' => __('Nav', 'dream'),
                'param_name' => 'nav',
                'value' => array(
                  __('Yes', 'dream') => '1',
                  __('No', 'dream') => '0',
                ),
                'std' => '0',
                'description' => __('Show next/prev buttons.', 'dream'),
                'group' => 'Slider Options',
              ),
              array(
                'type' => 'dropdown',
                'heading' => __('Dots', 'dream'),
                'param_name' => 'dots',
                'value' => array(
                  __('Yes', 'dream') => '1',
                  __('No', 'dream') => '0',
                ),
                'std' => '0',
                'description' => __('Show dots navigation.', 'dream'),
                'group' => 'Slider Options',
              ),
              array(
                'type' => 'textfield',
                'heading' => __('slideBy', 'dream'),
                'param_name' => __('slide_by', 'dream'),
                'value' => 1,
                'description' => __('Navigation slide by x. `page` string can be set to slide by page.', 'dream'),
                'group' => 'Slider Options',
              ),
              array(
                'type' => 'dropdown',
                'heading' => __('Autoplay', 'dream'),
                'param_name' => 'autoplay',
                'value' => array(
                  __('Yes', 'dream') => '1',
                  __('No', 'dream') => '0',
                ),
                'std' => '0',
                'description' => __('Autoplay.', 'dream'),
                'group' => 'Slider Options',
              ),
              array(
                'type' => 'dropdown',
                'heading' => __('autoplayHoverPause', 'dream'),
                'param_name' => 'autoplay_hover_pause',
                'value' => array(
                  __('Yes', 'dream') => '1',
                  __('No', 'dream') => '0',
                ),
                'std' => '0',
                'description' => __('Pause on mouse hover.', 'dream'),
                'group' => 'Slider Options',
              ),
              array(
                'type' => 'textfield',
                'heading' => __('autoplayTimeout', 'dream'),
                'param_name' => 'autoplay_timeout',
                'value' => '5000',
                'description' => __('Autoplay interval timeout.', 'dream'),
                'group' => 'Slider Options',
              ),
              array(
                'type' => 'textfield',
                'heading' => __('smartSpeed', 'dream'),
                'param_name' => 'smart_speed',
                'value' => '250',
                'description' => __('AutoplaySpeed Calculate. More info to come..', 'dream'),
                'group' => 'Slider Options',
              ),
              array(
                'type' => 'textfield',
                'heading' => __('Table Items', 'dream'),
                'param_name' => 'responsive_table_items',
                'value' => '1',
                'admin_label' => false,
                'description' => __('The number of items you want to see on the table screen.', 'dream'),
                'group' => 'Slider Options',
              ),
              array(
                'type' => 'textfield',
                'heading' => __('Mobile Items', 'dream'),
                'param_name' => 'responsive_mobile_items',
                'value' => '1',
                'admin_label' => false,
                'description' => __('The number of items you want to see on the mobile screen.', 'dream'),
                'group' => 'Slider Options',
              ),
              array(
                'type' => 'css_editor',
                'heading' => __( 'Css', 'dream' ),
                'param_name' => 'css',
                'group' => __( 'Design options', 'dream' ),
              ),
            ),
          )
        );
    }

    /**
  	 * Parses google_fonts_data and font_container_data to get needed css styles to markup
  	 *
  	 * @param $el_class
  	 * @param $css
  	 * @param $atts
  	 *
  	 * @since 1.0
  	 * @return array
  	 */
    public function getStyles($el_class, $css, $atts) {
      $styles = array();

      /**
  		 * Filter 'VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG' to change vc_custom_heading class
  		 *
  		 * @param string - filter_name
  		 * @param string - element_class
  		 * @param string - shortcode_name
  		 * @param array - shortcode_attributes
  		 *
  		 * @since 4.3
  		 */
  		$css_class = apply_filters( 'vc_give_forms_slider_filter_class', 'wpb_theme_custom_element wpb_give_forms_slider ' . $el_class . vc_shortcode_custom_css_class( $css, ' ' ), $this->settings['base'], $atts );

  		return array(
  			'css_class' => trim( preg_replace( '/\s+/', ' ', $css_class ) ),
  			'styles' => $styles,
  		);
    }

    public function _variables($form_id = null, $atts = array()) {
      $goal_option = get_post_meta( $form_id, '_give_goal_option', true );
      $form        = new Give_Donate_Form( $form_id );
      $goal        = $form->goal;
      $goal_format = get_post_meta( $form_id, '_give_goal_format', true );
      $income      = $form->get_earnings();
      $color       = get_post_meta( $form_id, '_give_goal_color', true );
      // set color if empty
      if(empty($color)) $color = '#01FFCC';
      $progress = ($goal === 0) ? 0 : round( ( $income / $goal ) * 100, 2 );

      if ( $income >= $goal ) { $progress = 100; }
      $class_none = '';
      if ( $goal_option == 'disabled' ) { $class_none = 'class-none'; }
      // Get formatted amount.
      $income = give_human_format_large_amount( give_format_amount( $income ) );
      $goal = give_human_format_large_amount( give_format_amount( $goal ) );

      $button_donate = implode('', array(
        '<div class="give-button-donate">',
          do_shortcode('[give_form id="'. $form_id .'" show_title="true" show_goal="false" show_content="none" display_style="button"]'),
        '</div>',
      ));

      $variable = array(
        '{post_excerpt}' => get_the_excerpt($form_id),
        '{class_none}'=>$class_none,
        '{id}' => $form_id,
        '{form_title}' => get_the_title($form_id),
        '{form_link}' => get_permalink($form_id),
        '{form_featured_image}' => get_template_directory_uri() . '/assets/images/image-default-2.jpg',
        '{color}' => $color,
        '{author_name}' => get_the_author(),
      	'{date}' => get_the_date('d M, Y',$form_id),
        '{donors_count}' => $form->get_sales(),
		    '{raised_layout4}' => $income ,
		    '{goal_layout4}' => $goal ,
        '{pricing_text_layout4}' => sprintf(
          __('Raised: %1$s <span class="bt-padd">.</span> Goal: %2$s', 'dream'),
          '<span class="income">' . apply_filters( 'give_goal_amount_raised_output', give_currency_filter( $income ) ) . '</span>',
          '<span class="goal-text">' . apply_filters( 'give_goal_amount_target_output', give_currency_filter( $goal ) ) . '</span>'),
		    '{pricing_text_layout3}' => sprintf(
          __('Raised: %1$s / Goal: %2$s', 'dream'),
          '<span class="income">' . apply_filters( 'give_goal_amount_raised_output', give_currency_filter( $income ) ) . '</span>',
          '<span class="goal-text">' . apply_filters( 'give_goal_amount_target_output', give_currency_filter( $goal ) ) . '</span>'),
        '{pricing_text}' => sprintf(
          __('%1$s of %2$s raised', 'dream'),
          '<span class="income">' . apply_filters( 'give_goal_amount_raised_output', give_currency_filter( $income ) ) . '</span>',
          '<span class="goal-text">' . apply_filters( 'give_goal_amount_target_output', give_currency_filter( $goal ) ) . '</span>'),
        '{percentage_text}' => sprintf(
          __( '%s%% funded', 'dream' ),
          '<span class="give-percentage">' . apply_filters( 'give_goal_amount_funded_percentage_output', round( $progress ) ) . '</span>'),

        '{goal_progress_bar_default}' => '',
        '{button_donate}' => $button_donate,
        '{none}' =>'',
      );
      //echo '<pre>' ;
      //var_dump($goal_option );
    //  echo '</pre>' ;
      //Sanity check - ensure form has goal set to output
      if ( empty( $form->ID )
      	|| ( is_singular( 'give_forms' ) && ! give_is_setting_enabled( $goal_option ) )
      	|| ! give_is_setting_enabled( $goal_option )
      	|| $goal == 0
      ) {
        $progressbar_style_1_attr = array(
          'class' => 'give-goal-progress-bar-none',
        );
        $variable['{goal_progress_bar_style_1}'] = '<div '. html_build_attributes($progressbar_style_1_attr) .'></div>';
      } else {
        $progressbar_style_default_attr = array(
          'class' => 'give-goal-progress-bar',
          'data-progressbar-svg' => json_encode(array(
            /* source */
            'shape' => 'circle', //'circle',
            'progressValue' => $progress,
            'color' => $color,
            'strokeWidth' => 20,
            'trailColor' => 'rgb(222, 222, 222)',
            'trailWidth' => 3,
            'easing' => 'easeInOut',
            'duration' => 1800,
            'textSetings' => '',
            'animateTransformSettings' => 'show',
            'delay' => 300,
            /* transform */
            'colorTransform' => $color,
            'strokeWidthTransform' => 20,
            /* text */
            'label' => '{percent}%',
            'text_color' => '#fff',
          )),
        );
        $progressbar_style_1_attr = array(
          'class' => 'give-goal-progress-bar',
          'data-progressbar-svg' => json_encode(array(
            /* source */
            'shape' => 'line', //'line',
            'progressValue' => $progress,
            'color' => $color,
            'strokeWidth' => 1,
            'trailColor' => 'rgb(222, 222, 222)',
            'trailWidth' => 1,
            'easing' => 'easeInOut',
            'duration' => 1800,
            'textSetings' => '',
            'animateTransformSettings' => 'show',
            'delay' => 300,
            // 'svgStyleWidth' => '100%',
            // 'svgStyleHeight' => '100%',
            /* transform */
            'colorTransform' => $color,
            'strokeWidthTransform' => 2,
            /* text */
            // 'textSetings' => 'show',
            // 'label' => '{percent}%',
            // 'text_color' => '#fff',
          )),
        );
        //echo '<pre>' ;
        //var_dump($none );
        //echo '</pre>' ;
        $variable['{goal_progress_bar_default}'] = '<div '. html_build_attributes($progressbar_style_default_attr) .'></div>';
        $variable['{goal_progress_bar_style_1}'] = '<div '. html_build_attributes($progressbar_style_1_attr) .'></div>';

      }

      /* check featured image exist */
      if ( has_post_thumbnail($form_id) ) {
        $variable['{form_featured_image}'] = get_the_post_thumbnail_url($form_id, $atts['image_size']);
      }

      return $variable;
    }

    public function _template($temp = 'default', $form_id = null, $atts = array()) {
      $params = $this->_variables($form_id, $atts);

      $output = '';
      $template = array();

      /* layout default */
      $template['default'] = implode('', array(
        '<div class="item-inner give-forms-slider-layout-default">',
          '<div class="featured-image">',
            '<img src="{form_featured_image}" alt="#">',
            '<div class="give-goal-progress-wrap {class_none}">',
              '{goal_progress_bar_default}',
              '<div class="give-price-wrap">{pricing_text}</div>',
            '</div>',
            // '<a class="readmore-btn" href="{form_link}" title="{form_title}"><span class="ion-ios-arrow-right"></span></a>',
            '{button_donate}',
          '</div>',
          '<div class="entry-content">',
            '<div class="meta-donor">'. __('Donor', 'dream') .' {donors_count}</div>',
            '<a href="{form_link}" class="title-link"><h4 class="title">{form_title}</h4></a>',
            '<div class="extra-meta">',
              '<div class="meta-item meta-author">{author_name}</div>',
              ' / ',
              '<div class="meta-item meta-date">{date}</div>',
            '</div>',
            '<a href="{form_link}" class="readmore-btn">'. __('Read More' , 'dream') .' <span class="ion-ios-arrow-thin-right"></span></a>',
          '</div>',
        '</div>',
      ));

      /* layout style 1 horizontal give forms  */
      $template['style-1'] = implode('', array(
        '<div class="item-inner give-forms-slider-layout-style-1">',
          '<div class="featured-image " style="background: url({form_featured_image}) center center / cover, #9E9E9E;">',
            //'<img src="{form_featured_image}" alt="#">',
            '<a class="readmore-link" href="{form_link}" title="View detail"><span class="ion-ios-arrow-right"></span></a>',
          '</div>',
          '<div class="entry-content ">',
            '<div class="entry-content-inner">',
              '<div class="meta-donor">'. __('Donor', 'dream') .' {donors_count}</div>',
              '<div class="give-goal-progress-wrap {class_none}">',
                '<div class="give-price-wrap">{pricing_text}</div>',
                '{goal_progress_bar_style_1}',
              '</div>',
              '<a href="{form_link}" class="title-link"><h4 class="title">{form_title}</h4></a>',
              '<div class="extra-meta">',
                '<div class="meta-item meta-author">{author_name}</div>',
                ' / ',
                '<div class="meta-item meta-date">{date}</div>',
              '</div>',
              '<a class="readmore-btn" href="{form_link}" title="{form_title}">'. __('Read More', 'dream') .' <span class="ion-ios-arrow-thin-right"></span></a>',
            '</div>',
          '</div>',
        '</div>',
      ));

      /* style-2 */
      $template['style-2'] = implode('', array(
        '<div class="item-inner give-forms-slider-layout-style-2">',
          '<div class="meta-donor">{donors_count} '. __('Donor', 'dream') .'</div>',
          '<div class="featured-image">',
            '<a href="{form_link}">',
              '<img src="{form_featured_image}" alt="#">',
            '</a>',
          '</div>',
          '<div class="entry-content ">',
            '<div class="entry-content-inner">',
              '<div class="give-goal-progress-wrap {class_none}">',
                '<div class="give-price-wrap">{pricing_text}</div>',
                '{goal_progress_bar_style_1}',
              '</div>',
              '<a href="{form_link}" class="title-link"><h4 class="title">{form_title}</h4></a>',
              '<div class="entry-bot">',
                '<a class="readmore-btn" href="{form_link}" title="{form_title}">'. __('Read More', 'dream') .' <span class="ion-ios-arrow-thin-right"></span></a>',
                '<div class="extra-meta">',
                  '<div class="meta-item meta-author">{author_name}</div>',
                  ' / ',
                  '<div class="meta-item meta-date">{date}</div>',
                '</div>',
              '</div>',
            '</div>',
          '</div>',
        '</div>',
      ));

	  /* style-3 */
      $template['style-3'] = implode('', array(
        '<div class="item-inner give-forms-slider-layout-style-3">',
          '<div class="featured-image">',
            '<a href="{form_link}">',
              '<img src="{form_featured_image}" alt="#">',
            '</a>',
          '</div>',
          '<div class="entry-content ">',
            '<div class="entry-content-inner">',
			  '<div class="extra-meta">',
                '<div class="meta-item meta-date"><span class="ion-android-calendar"></span>{date}</div>',
              '</div>',
			  '<a href="{form_link}" class="title-link"><h4 class="title">{form_title}</h4></a>',
              '<div class="give-goal-progress-wrap {class_none}">',
                '<div class="give-price-wrap">{pricing_text_layout3}</div>',
                '{goal_progress_bar_style_1}',
              '</div>',
              '<div class="entry-bot">',
                '<a class="readmore-btn" href="{form_link}" title="{form_title}"><span class="ion-log-in"></span>'. __('Read More', 'dream') .'</a>',
              '</div>',
            '</div>',
          '</div>',
        '</div>',
      ));

	  /* style-4 */
      $template['style-4'] = implode('', array(
        '<div class="item-inner give-forms-slider-layout-style-4">',
          '<div class="featured-image">',
            '<a href="{form_link}">',
              '<img src="{form_featured_image}" alt="#">',
            '</a>',
          '</div>',
          '<div class="entry-content ">',
            '<div class="entry-content-inner">',
			  '<div class="extra-meta">',
                '<div class="meta-item meta-date"><span class="ion-android-calendar"></span>{date}</div>',
              '</div>',
			  '<a href="{form_link}" class="title-link"><h4 class="title">{form_title}</h4></a>',
              '<div class="give-goal-progress-wrap {class_none}">',
                '{goal_progress_bar_style_1}',
                '<div class="bt-com">Donation Completed</div>',
              '</div>',
              '<div class="entry-bot">',
                '<div class="give-price-raised"><span>Raised</span><strong>$</strong>{raised_layout4}</div>',
                '<div class="give-price-goal"><span>Group Goal</span><strong>$</strong>{goal_layout4}</div>',
              '</div>',
            '</div>',
          '</div>',
        '</div>',
      ));

	   /* style-5 */
      $template['style-5'] = implode('', array(
        '<div class="item-inner give-forms-slider-layout-style-5">',
				'<div class="image-meta" style="background: url({form_featured_image}) no-repeat center center / cover, #333;">',
					'<div class="time-left-meta">{donors_count} '. __('Donor', 'dream') .'</div>',
				'</div>',
				'<div class="give-goal-progress-wrap {class_none}">',
					'<div class="give-price-wrap">{pricing_text}</div>',
					'{goal_progress_bar_style_1}',
				  '</div>',
				'<div class="info-meta">',
					'<a href="{form_link}" class="title-link"><h4 class="title">{form_title}</h4></a>',
					'<div class="button-meta">',
						'<div class="button-meta-inner">',
							'<a href="#" class="donate-button">{button_donate}</a>',
							'<a href="{form_link}" title="{form_title}" class="readmore-button" >'. __('Read More', 'dream') .'</a>',
						'</div>',
					'</div>',
				'</div>',
				'<div class="extra-meta">',
					'<div class="category"><i class="ion-ios-folder-outline"></i> {date}</div>',
					'<div class="author"><i class="ion-ios-person-outline"></i> {author_name}</div>',
				'</div>',
        '</div>',
      ));
      /* style-6 */
        $template['style-6'] = implode('', array(
          '<div class="item-inner give-forms-slider-layout-style-6">',
            '<div class="featured-image" style="background: url({form_featured_image}) no-repeat scroll center center / cover;">',
                '<div class="bt-overlay">',
                '</div>',
            '</div>',
            '<div class="entry-content ">',
              '<div class="entry-content-inner">',
  			        '<a href="{form_link}" class="title-link"><h4 class="title">{form_title}</h4></a>',
                '<div class="bt-excerpt">{post_excerpt}</div>',
                '<div class="give-goal-progress-wrap {class_none}">',
                  '{goal_progress_bar_style_1}',
                  '<div class="give-price-wrap">{pricing_text_layout4}</div>',
                '</div>',
                '<div class="entry-bot">',
                  '<a href="#" class="donate-button">{button_donate}</a>',
                '</div>',
              '</div>',
            '</div>',
          '</div>',
        ));
        /* style-7 */
          $template['style-7'] = implode('', array(
            '<div class="item-inner give-forms-slider-layout-style-7">',
              '<div class="featured-image" style="background: url({form_featured_image}) no-repeat scroll center center / cover;">',
                  '<div class="bt-overlay">',
                  '</div>',
              '</div>',
              '<div class="entry-content ">',
                '<div class="entry-content-inner">',
                  '<a href="{form_link}" class="title-link"><h4 class="title">{form_title}</h4></a>',
                  '<div class="extra-meta">',
          					'<div class="date"> {date}</div>',
          					'<div class="author">By {author_name}</div>',
          				'</div>',
                '</div>',
              '</div>',
            '</div>',
          ));
      /* layout blog-image */
      $template['block-image'] = implode('', array(

      ));

      $template = apply_filters('vc_give_forms_slider:template', $template);

      return str_replace(array_keys($params), array_values($params), fw_akg($temp, $template));
    }

    // Element HTML
    public function vc_give_forms_slider_html( $atts ) {
      $atts['self'] = $this;
      return fw_render_view(get_template_directory() . '/framework-customizations/extensions/custom-js-composer/vc-elements/vc_give_forms_slider.php', array('atts' => $atts), true);
    }

} // End Element Class


// Element Class Init
new vcGiveFormsSlider();
